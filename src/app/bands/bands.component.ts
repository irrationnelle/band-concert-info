import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Band } from '../band';
import { BandService } from '../band.service';

@Component({
  selector: 'app-bands',
  templateUrl: './bands.component.html',
  styleUrls: ['./bands.component.css'],
})
export class BandsComponent implements OnInit {
  bands: Band[] = [];

  constructor(
    private bandService: BandService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    const genre = this.route.snapshot.paramMap.get('genre');
    if (!genre || genre === '') {
      this.getBands();
    } else {
      this.getBandsWithGenre(genre);
    }
  }

  getBands(): void {
    this.bandService.getBands().subscribe(bands => (this.bands = bands));
  }

  getBandsWithGenre(genre: string): void {
    this.bandService
      .getBandsWithGenre(genre)
      .subscribe(band => (this.bands = [...this.bands, band]));
  }
}
