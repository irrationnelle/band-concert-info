import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here

import { AppComponent } from './app.component';
import { BandsComponent } from './bands/bands.component';
import { BandDetailComponent } from './band-detail/band-detail.component';
import { AppRoutingModule } from './app-routing.module';
import { MainComponent } from './main/main.component';
import { GenreComponent } from './genre/genre.component';

@NgModule({
  declarations: [AppComponent, BandsComponent, BandDetailComponent, MainComponent, GenreComponent],
  imports: [BrowserModule, FormsModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
