import { Component, OnInit } from '@angular/core';
import { Band } from '../band';
import { BandService } from '../band.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit {
  bands: Band[];

  constructor(private bandService: BandService) {}

  ngOnInit() {
    this.getBands();
  }

  getBands(): void {
    this.bandService
      .getBands()
      .subscribe(bands => (this.bands = bands.slice(1, 5)));
  }
}
