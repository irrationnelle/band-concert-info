import { Component, OnInit } from '@angular/core';

import { Band } from '../band';
import { BandService } from '../band.service';

@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.css'],
})
export class GenreComponent implements OnInit {
  genres: string[];

  constructor(private bandService: BandService) {}

  ngOnInit() {
    this.getGenres();
  }

  getGenres(): void {
    this.bandService.getGenres().subscribe(genres => (this.genres = genres));
  }
}
