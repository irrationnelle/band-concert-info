import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { filter, reduce } from 'rxjs/operators';

import { Band } from './band';
import { BANDS } from './mock-bands';

@Injectable({
  providedIn: 'root',
})
export class BandService {
  constructor() {}

  getBands(): Observable<Band[]> {
    return of(BANDS);
  }

  getBandsWithGenre(genre: string): Observable<Band> {
    return of(...BANDS).pipe(filter(band => band.genre === genre));
  }

  getBand(id: number): Observable<Band> {
    return of(BANDS.find(band => band.id === id));
  }

  getGenres(): Observable<string[]> {
    return of(...BANDS).pipe(
      reduce((acc: Array<string>, band: Band) => {
        if (!acc.includes(band.genre)) {
          acc = [...acc, band.genre];
        }
        return acc;
      }, [])
    );
  }
}
