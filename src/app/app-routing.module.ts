import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BandsComponent } from './bands/bands.component';
import { MainComponent } from './main/main.component';
import { BandDetailComponent } from './band-detail/band-detail.component';
import { GenreComponent } from './genre/genre.component';

const routes: Routes = [
  { path: '', redirectTo: '/main', pathMatch: 'full' },
  { path: 'main', component: MainComponent },
  { path: 'bands', component: BandsComponent },
  { path: 'genre', component: GenreComponent },
  { path: 'band/:id', component: BandDetailComponent },
  { path: 'bands/:genre', component: BandsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
