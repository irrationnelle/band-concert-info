import { Band } from './band';

export const BANDS: Band[] = [
  {
    id: 1,
    name: 'DARK MIRROR OV TRAGEDY',
    day: new Date(2018, 9, 21, 17, 0),
    place: '프리즘홀',
    genre: '메탈',
    price: 35000,
  },
  {
    id: 2,
    name: 'EYM Trio',
    day: new Date(2018, 9, 16, 20, 0),
    place: '재즈클럽 그루브',
    genre: '재즈',
    price: 25000,
  },
  {
    id: 3,
    name: '랜드마인',
    day: new Date(2018, 10, 10, 19, 0),
    place: '라이브클럽 인터플레이',
    genre: '메탈',
    price: 5000,
  },
  {
    id: 4,
    name: 'NOEAZY',
    day: new Date(2018, 9, 21, 17, 0),
    place: '프리즘홀',
    genre: '메탈',
    price: 35000,
  },
];
