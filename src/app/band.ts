export interface Band {
  id: number;
  name: string;
  day: Date;
  place: string;
  genre: string;
  price: number;
}
